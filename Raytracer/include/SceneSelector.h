#ifndef SCENESELECTOR_H
#define SCENESELECTOR_H

#include <memory>

#include "Hittable.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Forward declaration
class SceneBackground;
class SceneBackgroundEarthAndSky;

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Scene selection class
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class SceneSelector
{
public:
    enum ESceneType 
    {
        kSceneType_TestScene = 0u,
        kSceneType_RaytracingInOneWeekend,
        kSceneType_RaytracingInOneWeekendWithLights,
        kSceneType_NumOf
    };

    struct CameraConfiguration
    {
        Vec3 vViewPos;
        Vec3 vViewTarget;
        Real fAperture;
    };

public:
    SceneSelector();

public:
    void LoadScene(ESceneType _kSceneType, HittableList& _World) const;
    std::shared_ptr<SceneBackground> getBackground(ESceneType _kSceneType) const;
    CameraConfiguration getCameraConfiguration(ESceneType _kSceneType) const;
};

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Scene background helper classes
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class SceneBackground
{
public:
    SceneBackground() : m_vColor(Vec3(0.0, 0.0, 0.0)) {}
    SceneBackground(Vec3 _vColor) : m_vColor(_vColor) {}
public:
    virtual Vec3 Evaluate(const Ray& _Ray) const
    {
        return m_vColor;
    }
private:
    Vec3 m_vColor;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class SceneBackgroundEarthAndSky : public SceneBackground
{
public:
    SceneBackgroundEarthAndSky() {}
public:
    Vec3 Evaluate(const Ray& _Ray) const override
    {
        Vec3 vDirUnit = _Ray.getDirection().getUnitVec();
        Real fT = 0.5f * (vDirUnit.y + 1.0f);
        const Vec3 vWhite(1.0f, 1.0f, 1.0f);
        const Vec3 vBlue(0.5f, 0.7f, 1.0f);
        return lerp(vWhite, vBlue, fT);
    }
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------


#endif // SCENESELECTOR_H