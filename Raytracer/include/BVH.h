#ifndef BVH_H
#define BVH_H

#include "Hittable.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class BVH_Node : public Hittable
{
public:
    BVH_Node() = delete;
    BVH_Node(const HittableList& _List);
    BVH_Node(const std::vector<std::shared_ptr<Hittable>>& _Objects, size_t _uStart, size_t _uEnd);
    ~BVH_Node() override = default;
public:
    bool isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const final;
    bool getBoundingBox(AABB& _AABB) const final;

private:
    std::shared_ptr<Hittable> m_pLeft;
    std::shared_ptr<Hittable> m_pRight;
    AABB m_AABB;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // BVH_H