#ifndef MATERIAL_H
#define MATERIAL_H

#include "Hittable.h"
#include "Random.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Material
{
public:
	virtual bool Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const = 0;

public:
	virtual Vec3 Emitted() const { return Vec3(0.0,0.0,0.0); }
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Lambertian : public Material
{
public:
	Lambertian() = delete;
	Lambertian(const Vec3& _vAlbedoColor) : m_vAlbedoColor(_vAlbedoColor) {}
public:
	/// Possible implementations:
	/// 1) scatter always and attenuate by reflectance R
	/// 2) scatter with no attenuation, absorb fraction 1-R of the rays
	/// 3) scatter with probability p and attenuate with albedo/p
	bool Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const override;

private:
	Vec3 m_vAlbedoColor;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Metal : public Material
{
public:
	Metal() = delete;
	Metal(const Vec3& _vAlbedoColor, const Real _fRoughness) : m_vAlbedoColor(_vAlbedoColor), m_fRoughness(_fRoughness)
	{
		if(m_fRoughness > Real(1.0))
		{
			m_fRoughness = Real(1.0);
		}
	}
public:
	bool Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const override;

private:
	Vec3 m_vAlbedoColor;
	Real m_fRoughness;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Dielectric : public Material
{
public:
	Dielectric() = delete;
	Dielectric(const Real _fRefractionIndex) : m_fRefractionIndex(_fRefractionIndex) { }
public:
	bool Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const override;

private:
	Real m_fRefractionIndex;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // MATERIAL_H