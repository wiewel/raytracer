#ifndef RAY_H
#define RAY_H

#include "Base.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Ray
{
public:
	Ray() {}
	Ray(const Vec3& _vOrigin, const Vec3& _vDir) : m_vOrigin(_vOrigin), m_vDir(_vDir)
	{
		assertm(m_vDir.isCloseToZero() == false, "Direction must have non-zero values.");
	}

public:
	const Vec3& getOrigin() const { return m_vOrigin; }
	const Vec3& getDirection() const { return m_vDir; }
	Vec3 getPointAtParameter(const Real& _fParam) const { return m_vOrigin + m_vDir * _fParam; }

private:
	Vec3 m_vOrigin;
	Vec3 m_vDir;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // RAY_H