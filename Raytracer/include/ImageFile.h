#ifndef IMAGEFILE_H
#define IMAGEFILE_H

#include "Base.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
struct ColorRGB {
	ColorRGB(float r, float g, float b) :
		r(r),
		g(g),
		b(b)
	{}

	ColorRGB(const Vec3& _Vec) :
		r(_Vec.r),
		g(_Vec.g),
		b(_Vec.b)
	{}
	float r, g, b;
};
struct ColorXY {
	ColorXY(float x, float y) :
		x(x),
		y(y)
	{}
	float x, y;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Base image class
template <class T> 
class ImageFile
{
public:
	ImageFile(int _iResX, int _iResY, int _iChannels) :
		m_iResX(_iResX),
		m_iResY(_iResY),
		m_iChannels(_iChannels)
	{
		m_Data.reserve(static_cast<size_t>(m_iResX * m_iResY));
	}

public:
	// fill the image with content
	void Fill(std::vector<T>& _Data);
	
	// Write to file
	virtual void Write(const std::string& _sFilePath) const = 0;

protected:
	int m_iResX;
	int m_iResY;
	int m_iChannels;

	std::vector<T> m_Data;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// PPM class
class PPM : public ImageFile<ColorRGB>
{
public:
	PPM(int _iResX, int _iResY, int _iMaxColor = 255) :
		ImageFile(_iResX, _iResY, 3),
		m_iMaxColor(_iMaxColor)
	{
	}

public:
	void Fill(std::vector<ColorRGB>& _Data);
	void Write(const std::string& _sFilePath) const;

private:
	int m_iMaxColor;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif //IMAGEFILE_H