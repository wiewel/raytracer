#ifndef AABB_H
#define AABB_H

#include "Ray.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class AABB 
{
public:
    AABB() : m_vMinimum(0,0,0), m_vMaximum(0,0,0) {};
    AABB(const Vec3& _vMinimum, const Vec3& _vMaximum) : m_vMinimum(_vMinimum), m_vMaximum(_vMaximum)
    {
        assertm( m_vMinimum.x < m_vMaximum.x && m_vMinimum.y < m_vMaximum.y && m_vMinimum.z < m_vMaximum.z, "AABB: All elements of minimum must be less than elements of maximum!");
    }
    ~AABB() = default;

public:
    bool isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT) const;
    void combineWith(const AABB& _Other);

public:
    Vec3 getMin() const { return m_vMinimum; };
    Vec3 getMax() const { return m_vMaximum; };

    Vec3& getMin() { return m_vMinimum; };
    Vec3& getMax() { return m_vMaximum; };

private:
    Vec3 m_vMinimum;
    Vec3 m_vMaximum;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // AABB_H