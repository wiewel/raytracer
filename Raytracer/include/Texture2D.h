#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <memory>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Texture2D
{
public:
    Texture2D();
    ~Texture2D();

    /// Delete copy constructor and assignment
    Texture2D(const Texture2D&) = delete;
    Texture2D& operator=(const Texture2D&) = delete;

    Texture2D(Texture2D&& _OtherTexture);
    Texture2D& operator=(Texture2D&& _OtherTexture);

public:
    void Create(unsigned int _uResX, unsigned int _uResY, unsigned int _uFormat = GL_RGBA, unsigned int _uType = GL_UNSIGNED_BYTE, const GLubyte* _pInitialData = nullptr);
    void Activate(unsigned int _uTextureUnitStage); // e.g. GL_TEXTURE0

private:
    void Release();

private:
    GLuint m_uID = 0u;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Texture2D::Texture2D(Texture2D&& _OtherTexture) : m_uID(_OtherTexture.m_uID)
{
    _OtherTexture.m_uID = 0u;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Texture2D& Texture2D::operator=(Texture2D&& _OtherTexture)
{
    if (this != &_OtherTexture)
    {
        Release();
        std::swap(m_uID, _OtherTexture.m_uID);
    }
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // TEXTURE2D_H