#ifndef VEC3_H
#define VEC3_H

#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <algorithm>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
template <class T>
class Vector3 
{
public:
	Vector3() { std::fill_n(m_Data, 3, T()); }
	Vector3(T _e0, T _e1, T _e2) : m_Data {_e0,_e1,_e2} {}

    Vector3(const Vector3<T>& _Other) = default;
    Vector3<T>& operator=(const Vector3<T>& _Other) = default;

    Vector3(Vector3<T>&& _Other) = default;
    Vector3<T>& operator=(Vector3<T>&& _Other) = default;

public:
    const Vector3<T>& operator+() const { return *this; }
    Vector3<T> operator-() const { return Vector3<T>(-m_Data[0], -m_Data[1], -m_Data[2]); }
    T operator[](int i) const { return m_Data[i]; }
    T& operator[](int i) { return m_Data[i]; }

    Vector3<T>& operator+= (const Vector3<T>& _vOther);
    Vector3<T>& operator-= (const Vector3<T>& _vOther);
    Vector3<T>& operator*= (const Vector3<T>& _vOther);
    Vector3<T>& operator/= (const Vector3<T>& _vOther);
    Vector3<T>& operator+= (const T& _t);
    Vector3<T>& operator-= (const T& _t);
    Vector3<T>& operator*= (const T& _t);
    Vector3<T>& operator/= (const T& _t);

    Vector3<T> operator+ (const Vector3<T>& _vOther) const;
    Vector3<T> operator- (const Vector3<T>& _vOther) const;
    Vector3<T> operator* (const Vector3<T>& _vOther) const;
    Vector3<T> operator/ (const Vector3<T>& _vOther) const;
    Vector3<T> operator+ (const T& _t) const;
    Vector3<T> operator- (const T& _t) const;
    Vector3<T> operator* (const T& _t) const;
    Vector3<T> operator/ (const T& _t) const;
 
public:
    T getLength() const { return sqrt(m_Data[0] * m_Data[0] + m_Data[1] * m_Data[1] + m_Data[2] * m_Data[2]); }
    T getSquaredLength() const { return m_Data[0] * m_Data[0] + m_Data[1] * m_Data[1] + m_Data[2] * m_Data[2]; }

public:
    Vector3<T> getUnitVec() const;
    void makeUnitVec();

    bool isCloseToZero() const;

    /// _vNormal needs to be a unit vector!
    Vector3<T> getReflectedVector(const Vector3<T>& _vNormal) const;
    void reflectOnNormal(const Vector3<T>& _vNormal);

    T dot(const Vector3<T>& _vOther) const;
    Vector3<T> cross(const Vector3<T>& _vOther) const;

public:
    union
    {
        struct {
            T x;
            T y;
            T z;
        };
        struct {
            T r;
            T g;
            T b;
        };
        T m_Data[3];
    };
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator+=(const Vector3<T>& _vOther)
{
    m_Data[0] += _vOther.m_Data[0];
    m_Data[1] += _vOther.m_Data[1];
    m_Data[2] += _vOther.m_Data[2];
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator-=(const Vector3<T>& _vOther)
{
    m_Data[0] -= _vOther.m_Data[0];
    m_Data[1] -= _vOther.m_Data[1];
    m_Data[2] -= _vOther.m_Data[2];
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator*=(const Vector3<T>& _vOther)
{
    m_Data[0] *= _vOther.m_Data[0];
    m_Data[1] *= _vOther.m_Data[1];
    m_Data[2] *= _vOther.m_Data[2];
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator/=(const Vector3<T>& _vOther)
{
    m_Data[0] /= _vOther.m_Data[0];
    m_Data[1] /= _vOther.m_Data[1];
    m_Data[2] /= _vOther.m_Data[2];
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator+=(const T& _t)
{
    m_Data[0] += _t;
    m_Data[1] += _t;
    m_Data[2] += _t;
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator-=(const T& _t)
{
    m_Data[0] -= _t;
    m_Data[1] -= _t;
    m_Data[2] -= _t;
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator*=(const T& _t)
{
    m_Data[0] *= _t;
    m_Data[1] *= _t;
    m_Data[2] *= _t;
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T>& Vector3<T>::operator/=(const T& _t)
{
    const T tInv = T(1.0) / _t;
    m_Data[0] *= tInv;
    m_Data[1] *= tInv;
    m_Data[2] *= tInv;
    return *this;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator+(const Vector3<T>& _vOther) const
{
    return Vector3<T>(m_Data[0] + _vOther.m_Data[0], m_Data[1] + _vOther.m_Data[1], m_Data[2] + _vOther.m_Data[2]);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator-(const Vector3<T>& _vOther) const
{
    return Vector3<T>(m_Data[0] - _vOther.m_Data[0], m_Data[1] - _vOther.m_Data[1], m_Data[2] - _vOther.m_Data[2]);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator*(const Vector3<T>& _vOther) const
{
    return Vector3<T>(m_Data[0] * _vOther.m_Data[0], m_Data[1] * _vOther.m_Data[1], m_Data[2] * _vOther.m_Data[2]);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator/(const Vector3<T>& _vOther) const
{
    return Vector3<T>(m_Data[0] / _vOther.m_Data[0], m_Data[1] / _vOther.m_Data[1], m_Data[2] / _vOther.m_Data[2]);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator+(const T& _t) const
{
    return Vector3<T>(m_Data[0] + _t, m_Data[1] + _t, m_Data[2] + _t);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator-(const T& _t) const
{
    return Vector3<T>(m_Data[0] - _t, m_Data[1] - _t, m_Data[2] - _t);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator*(const T& _t) const
{
    return Vector3<T>(m_Data[0] * _t, m_Data[1] * _t, m_Data[2] * _t);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::operator/(const T& _t) const
{
    const T tInv = T(1.0) / _t;
    return Vector3<T>(m_Data[0] * tInv, m_Data[1] * tInv, m_Data[2] * tInv);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Global Operators
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
Vector3<T> operator * (T _t, const Vector3<T>& _Vec)
{
    return _Vec * _t;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
Vector3<T> operator / (T _t, const Vector3<T>& _Vec)
{
    return _Vec / _t;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Functions
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template <>
inline float Vector3<float>::getLength() const
{
    return sqrtf(m_Data[0] * m_Data[0] + m_Data[1] * m_Data[1] + m_Data[2] * m_Data[2]);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::getUnitVec() const
{
    return *this / this->getLength();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline void Vector3<T>::makeUnitVec()
{
    *this /= this->getLength();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline bool Vector3<T>::isCloseToZero() const
{
    const Real kEpsilon = Real(1e-8);
    return std::abs(x) < kEpsilon && std::abs(y) < kEpsilon && std::abs(z) < kEpsilon;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::getReflectedVector(const Vector3<T>& _vNormal) const
{
    return *this - (_vNormal * 2.0 * (this->dot(_vNormal)));
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline void Vector3<T>::reflectOnNormal(const Vector3<T>& _vNormal)
{
    *this -= 2.0 * this->dot(_vNormal) * _vNormal;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline T Vector3<T>::dot(const Vector3<T>& _vOther) const
{
    return m_Data[0] * _vOther.m_Data[0] + m_Data[1] * _vOther.m_Data[1] + m_Data[2] * _vOther.m_Data[2];
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template<class T>
inline Vector3<T> Vector3<T>::cross(const Vector3<T>& _vOther) const
{
    return Vector3<T>( m_Data[1] * _vOther.m_Data[2] - m_Data[2] * _vOther.m_Data[1],
                    m_Data[2] * _vOther.m_Data[0] - m_Data[0] * _vOther.m_Data[2],
                    m_Data[0] * _vOther.m_Data[1] - m_Data[1] * _vOther.m_Data[0] );
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Helper Functions
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template <class T>
inline std::istream& operator>>(std::istream& _InputStream, Vector3<T>& _Vec) {
    _InputStream >> _Vec.m_Data[0] >> _Vec.m_Data[1] >> _Vec.m_Data[2];
    return _InputStream;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
template <class T>
inline std::ostream& operator<<(std::ostream& _OutputStream, const Vector3<T>& _Vec) {
    _OutputStream << _Vec.m_Data[0] << " " << _Vec.m_Data[1] << " " << _Vec.m_Data[2];
    return _OutputStream;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif //VEC3_H