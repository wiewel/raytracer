#ifndef SPHERE_H
#define SPHERE_H

#include "Hittable.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Sphere : public Hittable
{
public:
    Sphere() = delete;
    Sphere(const Vec3& _vCenter, Real _fRadius, const std::shared_ptr<Material>& _pMaterial) : m_vCenter(_vCenter), m_fRadius(_fRadius), m_pMaterial(_pMaterial)
    {
        assertm(m_pMaterial != nullptr, "Sphere: A material must be given!");
    }
    ~Sphere() override = default;
public:
    bool isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const final;
    bool getBoundingBox(AABB& _AABB) const final;

private:
    Vec3 m_vCenter;
    Real m_fRadius;
    std::shared_ptr<Material> m_pMaterial;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // SPHERE_H