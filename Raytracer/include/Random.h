#ifndef RANDOM_H
#define RANDOM_H

#include "Base.h"
#include <functional>
#include <random>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Project plane sample to hemisphere
inline Vec3 getPointOnHemisphere(const Real _fU1, const Real _fU2)
{
    Real fR = sqrt( Real(1.0) - _fU1 * _fU1);
    Real fPhi = Real(2.0) * kPi * _fU2;
    return Vec3(cos(fPhi) * fR, sin(fPhi) * fR, _fU1);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline double getRandomDouble()
{
    static std::uniform_real_distribution<double> RandomDist(0.0, 1.0);
    static std::mt19937 Gen;
    static std::function<double()> RandGen = std::bind(RandomDist, Gen);
    return RandGen();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Real getRandomReal()
{
    static std::uniform_real_distribution<Real> RandomDist(0.0, 1.0);
    static std::mt19937 Gen;
    static std::function<Real()> RandGen = std::bind(RandomDist, Gen);
    return RandGen();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline double getRandomDouble(double _fMin, double _fMax)
{
    return getRandomDouble() * (_fMax - _fMin) + _fMin;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Real getRandomReal(Real _fMin, Real _fMax)
{
    return static_cast<Real>(getRandomDouble(_fMin, _fMax));
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline int getRandomInt(int _iMin, int _iMax)
{
    return static_cast<int>(getRandomReal(Real(_iMin), Real(_iMax + 1)));
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomPointOnHemisphere()
{
    return getPointOnHemisphere(getRandomReal(), getRandomReal());
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomPointInUnitSphere()
{
    Real fTheta = Real(2.0) * kPi * getRandomReal();
    Real fPhi = acos(Real(1.0) - Real(2.0) * getRandomReal());
    Real fPhiSin = sin(fPhi);
    Real fX = fPhiSin * cos(fTheta);
    Real fY = fPhiSin * sin(fTheta);
    Real fZ = cos(fPhi);
    return Vec3(fX, fY, fZ) * std::cbrt(getRandomReal());
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomPointOnUnitDisk()
{
    Real fR = sqrt(getRandomReal());
    Real fTheta = getRandomReal() * Real(2.0) * kPi;
    return Vec3( fR * cos(fTheta), fR * sin(fTheta), 0.0 );
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomUnitVec3()
{
    return getRandomPointInUnitSphere().getUnitVec();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomVec3()
{
    return Vec3(getRandomReal(), getRandomReal(), getRandomReal());
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 getRandomVec3(Real _fMin, Real _fMax)
{
    return Vec3(getRandomReal(_fMin, _fMax), getRandomReal(_fMin, _fMax), getRandomReal(_fMin, _fMax));
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // RANDOM_H