#ifndef THREADING_H
#define THREADING_H

#include <thread>
#include <atomic>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class SpinLock
{
public:
	SpinLock(bool _bAcquire = false)
	{
		if(_bAcquire)
		{
			AcquireLock();
		}
	}
	~SpinLock()
	{
		ReleaseLock();
	}
	
public:
	void AcquireLock()
	{
		while (m_SpinLock.test_and_set(std::memory_order_acquire)); /// Spin and lock
	}
	void ReleaseLock()
	{
		m_SpinLock.clear(std::memory_order_release);
	}

private:
	std::atomic_flag m_SpinLock = ATOMIC_FLAG_INIT;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // THREADING_H