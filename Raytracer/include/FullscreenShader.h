#ifndef FULLSCREENSHADER_H
#define FULLSCREENSHADER_H

#include <string>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class FullscreenShader
{
public:
	FullscreenShader();
	~FullscreenShader();

	FullscreenShader(const FullscreenShader& _Other) = delete;
	FullscreenShader& operator=(const FullscreenShader& _Other) = delete;

	FullscreenShader(FullscreenShader&& _Other) = delete;
	FullscreenShader& operator=(FullscreenShader&& _Other) = delete;

public:
	void Activate(unsigned int _uTexUnit = 0u);
	void Render();

private:
	const char* GetVertexShader();
	const char* GetFragmentShader();

private:
	unsigned int m_uShaderProgram = 0u;
	unsigned int m_uTexLocation = 0u;
	unsigned int m_VAO = 0u;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // FULLSCREENSHADER_H