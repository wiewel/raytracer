#ifndef BASE_H
#define BASE_H

// #define NDEBUG
#include <cassert>
// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

#include <vector>
#include <cstdint>
#include <iostream>
#include <limits>
#include <memory>

/// Basetypes
using Real = float;
using uint = unsigned int;

#include "Vec3.h"

using Vec3 = Vector3<Real>;

/// Constants
const Real kPi = static_cast<Real>(3.14159265358979323846);

#endif