#ifndef CAMERA_H
#define CAMERA_H

#include "Ray.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Camera
{
public:
	Camera() = delete;
	Camera(const Vec3& _vOrigin, const Vec3& _vDirection, Real _fVerticalFoV, Real _fAspectRatio, Real _fAperture, Real _fFocusDist);

	Camera(const Camera& _Other) = default;
	Camera& operator=(const Camera& _Other) = default;

	Camera(Camera&& _Other) = default;
	Camera& operator=(Camera&& _Other) = default;

public:
	Ray getRay(Real _fXUV, Real _fYUV) const;

public:
	void setPosition(const Vec3& _vNewPosition);
	const Vec3& getPosition() const;
	void setDirection(const Vec3& _vNewDirection);
	const Vec3& getDirection() const;

private:
	void RecalculateInternals();

private:
	Vec3 m_vCornerBottomLeft;
	Vec3 m_vHorizontal;
	Vec3 m_vVertical;
	Vec3 m_vOrigin;

	Vec3 m_vU;
	Vec3 m_vV;
	Vec3 m_vW;

	Vec3 m_vDirection;
	Vec3 m_vUp;

	Real m_fVerticalFoV;
	Real m_fAspectRatio;
	Real m_fLensRadius;
	Real m_fFocusDist;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline void Camera::setPosition(const Vec3& _vNewPosition)
{
	m_vOrigin = _vNewPosition;
	RecalculateInternals();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline void Camera::setDirection(const Vec3& _vNewDirection)
{
	m_vDirection = _vNewDirection;
	RecalculateInternals();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline const Vec3& Camera::getPosition() const
{
	return m_vOrigin;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline const Vec3& Camera::getDirection() const
{
	return m_vDirection;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // CAMERA_H