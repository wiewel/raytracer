#ifndef HITTABLE_H
#define HITTABLE_H

#include "Ray.h"
#include "AABB.h"

class Material;

/// ------------------------------------------------------------------------------------------------------------------------------------------------
struct HitInfo
{
    Real fT;
    Vec3 vHitPosition;
    Vec3 vHitNormal;
    std::weak_ptr<Material> pMaterial;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class Hittable
{
public:
    virtual ~Hittable() = 0;
public:
    virtual bool isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const = 0;
    virtual bool getBoundingBox( AABB& _AABB ) const = 0;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------
class HittableList : public Hittable
{
public:
    HittableList() {}
    HittableList(const std::vector<std::shared_ptr<Hittable>>& _HittableList) : m_HittableList(_HittableList) {}
    ~HittableList() override {}

public:
    bool isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const final;
    bool getBoundingBox(AABB& _AABB) const final;

    const std::vector<std::shared_ptr<Hittable>>& getObjects() const { return m_HittableList; }

public:
    void AddObject(std::shared_ptr<Hittable> _pHittable);

private:
    std::vector<std::shared_ptr<Hittable>> m_HittableList;
};
/// ------------------------------------------------------------------------------------------------------------------------------------------------

#endif // HITTABLE_H