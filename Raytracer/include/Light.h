#ifndef LIGHT_H
#define LIGHT_H

#include "Material.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
class DiffuseLight : public Material
{
public:
	DiffuseLight() = delete;
	DiffuseLight(const Vec3& _vColor) : m_vColor(_vColor) {}
public:
	bool Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const override;
	Vec3 Emitted() const override;
	
private:
	Vec3 m_vColor;
};


#endif // LIGHT_H