#include "BVH.h"
#include "Random.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Constructors
/// ------------------------------------------------------------------------------------------------------------------------------------------------
BVH_Node::BVH_Node(const HittableList& _List) : BVH_Node(_List.getObjects(), 0u, _List.getObjects().size())
{
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Space partitioning is implemented in the constructor
/// 1) Choose axis
/// 2) sort primitives
/// 3) split primitives in half and assign to subtrees
BVH_Node::BVH_Node(const std::vector<std::shared_ptr<Hittable>>& _Objects, size_t _uStart, size_t _uEnd)
{
	/// Copy object pointer vector to modifiable version
	std::vector<std::shared_ptr<Hittable>> Objects = _Objects;

	/// Split primitives in half and assign to subtrees
	/// Nodes actually store pointers to objects in the leaf
	struct AxisSortComparator
	{
		uint m_uAxis = 0u;
		bool operator()(const std::shared_ptr<Hittable> _a, const std::shared_ptr<Hittable> _b) const
		{
			// Check for valid bounding boxes
			AABB a, b;
			bool bValidAABB = _a->getBoundingBox(a);
			bValidAABB &= _b->getBoundingBox(b);
			assertm(bValidAABB, "BVH Creation: child bounding boxes are invalid in comparator");

			// Make sure that axis is within bounds
			uint uAxis = std::min(m_uAxis, 2u);

			return a.getMin()[uAxis] < b.getMin()[uAxis];
		}
	};

	/// Select random axis
	AxisSortComparator Comp;
	Comp.m_uAxis = static_cast<uint>(getRandomInt(0, 2));

	size_t uObjSpan = _uEnd - _uStart;

	// If only one object is in span assign it to both sides for simplicity
	if (uObjSpan == 1)
	{
		m_pLeft = _Objects[_uStart];
		m_pRight = _Objects[_uStart];
	}
	// Early out if only two objects are in span
	else if (uObjSpan == 2)
	{
		if (Comp(_Objects[_uStart], _Objects[_uStart + 1]))
		{
			m_pLeft = _Objects[_uStart];
			m_pRight = _Objects[_uStart + 1];
		}
		else
		{
			m_pLeft = _Objects[_uStart + 1];
			m_pRight = _Objects[_uStart];
		}
	}
	else
	{
		std::sort(Objects.begin() + _uStart, Objects.begin() + _uEnd, Comp);
		size_t uMid = _uStart + uObjSpan / 2u;
		m_pLeft = std::make_shared<BVH_Node>(Objects, _uStart, uMid);
		m_pRight = std::make_shared<BVH_Node>(Objects, uMid, _uEnd);
	}

	/// Assign AABB
	AABB LeftAABB, RightAABB;
	bool bValidAABB = m_pLeft->getBoundingBox(LeftAABB);
	bValidAABB &= m_pRight->getBoundingBox(RightAABB);

	assertm(bValidAABB, "BVH Creation: child bounding boxes are invalid");

	m_AABB = LeftAABB;
	m_AABB.combineWith(RightAABB);
}

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Member Functions
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool BVH_Node::isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const
{
	if (m_AABB.isHitting(_Ray, _fMinT, _fMaxT) == false)
		return false;

	bool bHitLeft  = m_pLeft->isHitting(_Ray, _fMinT, _fMaxT, _HitInfo);
	bool bHitRight = m_pRight->isHitting(_Ray, _fMinT, bHitLeft ? _HitInfo.fT : _fMaxT, _HitInfo);

	return bHitLeft || bHitRight;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool BVH_Node::getBoundingBox(AABB& _AABB) const
{
	_AABB = m_AABB;
	return true;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------


