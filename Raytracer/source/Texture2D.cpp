#include "Texture2D.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
Texture2D::Texture2D()
{
    glGenTextures(1, &m_uID);
    glBindTexture(GL_TEXTURE_2D, m_uID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
Texture2D::~Texture2D()
{
	Release();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void Texture2D::Release()
{
    glDeleteTextures(1, &m_uID);
    m_uID = 0u;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void Texture2D::Create(unsigned int _uResX, unsigned int _uResY, unsigned int _uFormat, unsigned int _uType, const GLubyte* _pInitialData)
{
    glBindTexture(GL_TEXTURE_2D, m_uID);
    glTexImage2D(GL_TEXTURE_2D, 0, _uFormat, _uResX, _uResY, 0, _uFormat, _uType, _pInitialData);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void Texture2D::Activate(unsigned int _uTextureUnitStage)
{
    glActiveTexture(_uTextureUnitStage);
    glBindTexture(GL_TEXTURE_2D, m_uID);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
