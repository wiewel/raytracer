#include "Hittable.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
Hittable::~Hittable()
{
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool HittableList::isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const
{
    HitInfo CurrentHit;
    bool bHit = false;
    Real fCurrentT = _fMaxT;
    for (const auto& elem : m_HittableList)
    {
        if (elem->isHitting(_Ray, _fMinT, fCurrentT, CurrentHit))
        {
            bHit = true;
            fCurrentT = CurrentHit.fT;
            _HitInfo = CurrentHit;
        }
    }
    return bHit;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool HittableList::getBoundingBox(AABB& _AABB) const
{
    bool bHasAABB = false;
    for (const auto& elem : m_HittableList)
    {
        AABB CurrentAABB;
        if (elem->getBoundingBox(CurrentAABB))
        {
            // Adjust minimum and maximum of final AABB
            _AABB.combineWith(CurrentAABB);
            // Notify caller that this hittable list has a AABB
            bHasAABB = true;
        }
    }
    return bHasAABB;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void HittableList::AddObject(std::shared_ptr<Hittable> _pHittable)
{
    m_HittableList.push_back(_pHittable);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
