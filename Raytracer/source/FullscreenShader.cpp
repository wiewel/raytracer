#include "FullscreenShader.h"

#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <iostream>

/// ------------------------------------------------------------------------------------------------------------------------------------------------
FullscreenShader::FullscreenShader()
{
	/// Vertex shader creation
	GLuint uVertexShader = 0u;
	const char* VertexShader_FullscreenQuad = GetVertexShader();
	uVertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(uVertexShader, 1, &VertexShader_FullscreenQuad, NULL);
	glCompileShader(uVertexShader);

	/// Fragment shader creation
	GLuint uFragementShader = 0u;
	const char* FragmentShader_FullscreenQuad = GetFragmentShader();
	uFragementShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(uFragementShader, 1, &FragmentShader_FullscreenQuad, NULL);
	glCompileShader(uFragementShader);

	/// Generate dummy VAO for Mac OS
	glGenVertexArrays(1, &m_VAO);

	/// Shader linking
	m_uShaderProgram = glCreateProgram();
	glAttachShader(m_uShaderProgram, uVertexShader);
	glAttachShader(m_uShaderProgram, uFragementShader);
	glLinkProgram(m_uShaderProgram);

	/// Cleanup shaders since they are now stored in shader program
	glDeleteShader(uVertexShader);
	glDeleteShader(uFragementShader);

	/// Query texture location
	m_uTexLocation = glGetUniformLocation(m_uShaderProgram, "tex");
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
FullscreenShader::~FullscreenShader()
{
	glUseProgram(0u);
	glDeleteProgram(m_uShaderProgram);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void FullscreenShader::Activate(unsigned int _uTexUnit)
{
	glUseProgram(m_uShaderProgram);

	/// Link shader m_uTexLocation to previously bound tex unit
	glUniform1i(m_uTexLocation, _uTexUnit); // set it manually

	/// Bind VAO for Mac OS
	glBindVertexArray(m_VAO);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void FullscreenShader::Render()
{
	/// Draw fullscreen triangle
	glDrawArrays(GL_TRIANGLES, 0, 3);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
const char* FullscreenShader::GetVertexShader()
{
	static const char* VertexShader_FullscreenQuad =
		"#version 410\n"
		"out vec2 texCoord;\n"
		"void main()\n"
		"{\n"
		"    float x = -1.0 + float((gl_VertexID & 1) << 2);\n"
		"    float y = -1.0 + float((gl_VertexID & 2) << 1);\n"
		"    texCoord.x = (x+1.0)*0.5;\n"
		"    texCoord.y = (y+1.0)*0.5;\n"
		"    gl_Position = vec4(x, y, 0, 1);\n"
		"}\n";
	return VertexShader_FullscreenQuad;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
const char* FullscreenShader::GetFragmentShader()
{
	static const char* FragmentShader_FullscreenQuad =
		"#version 410\n"
		"in vec2 texCoord;\n"
		"uniform sampler2D tex;\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		"    FragColor = vec4(texture(tex, texCoord).rgb, 1.0);\n"
		"}\n";
	return FragmentShader_FullscreenQuad;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
