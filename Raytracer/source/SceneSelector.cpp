#include <memory>

#include "Vec3.h"
#include "Ray.h"
#include "MathFunc.h"
#include "Sphere.h"
#include "Random.h"

#include "Material.h"
#include "BVH.h"
#include "Light.h"
#include "SceneSelector.h"


/// ------------------------------------------------------------------------------------------------------------------------------------------------
void generateTestScene(HittableList& _World)
{
    /// Create some materials
    std::shared_ptr<Lambertian> LambMat0 = std::make_shared<Lambertian>(Vec3(0.8f, 0.3f, 0.3f));
    std::shared_ptr<Lambertian> LambMat1 = std::make_shared<Lambertian>(Vec3(0.8f, 0.8f, 0.0f));
    std::shared_ptr<Lambertian> LambBlue = std::make_shared<Lambertian>(Vec3(0.0f, 0.0f, 1.0f));
    std::shared_ptr<Lambertian> LambRed = std::make_shared<Lambertian>(Vec3(1.0f, 0.0f, 0.0f));
    std::shared_ptr<Metal>      MetalMat0 = std::make_shared<Metal>(Vec3(0.8f, 0.6f, 0.2f), 1.0f);
    std::shared_ptr<Metal>      MetalMat1 = std::make_shared<Metal>(Vec3(0.8f, 0.8f, 0.8f), 0.3f);
    std::shared_ptr<Dielectric> DielectricMat0 = std::make_shared<Dielectric>(1.5f);

    _World.AddObject(std::make_shared<Sphere>(Vec3(0.0f, 0.0f, -1.0f), 0.5f, LambMat0));
    _World.AddObject(std::make_shared<Sphere>(Vec3(0.0f, -100.5f, -1.0f), 100.0f, LambMat1));
    _World.AddObject(std::make_shared<Sphere>(Vec3(1.0f, 0.0f, -1.0f), 0.5f, MetalMat0));
    _World.AddObject(std::make_shared<Sphere>(Vec3(-1.0f, 0.0f, -1.0f), 0.5f, DielectricMat0));
    _World.AddObject(std::make_shared<Sphere>(Vec3(-1.0f, 0.0f, -1.0f), -0.45f, DielectricMat0));
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void generateRTinOneWeekendScene(HittableList& _World)
{
    HittableList SceneObjects;

    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(0.0f, -1000.0f, 0.0f), 1000.0f, std::make_shared<Lambertian>(Vec3(0.5, 0.5, 0.5))));

    int i = 1;
    for (int a = -11; a < 11; a++)
    {
        for (int b = -11; b < 11; b++)
        {
            auto choose_mat = getRandomReal();
            Vec3 center(a + 0.9f * getRandomReal(), 0.2f, b + 0.9f * getRandomReal());
            if ((center - Vec3(4.f, 0.2f, 0.f)).getLength() > 0.9f)
            {
                if (choose_mat < 0.8f)
                {
                    // diffuse
                    auto albedo = getRandomVec3() * getRandomVec3();
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Lambertian>(albedo)));
                }
                else if (choose_mat < 0.95f)
                {
                    // metal
                    auto albedo = getRandomVec3(.5f, 1.f);
                    auto fuzz = getRandomReal(0.f, .5f);
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Metal>(albedo, fuzz)));
                }
                else
                {
                    // glass
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Dielectric>(1.5f)));
                }
            }
        }
    }

    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(0.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Dielectric>(1.5f)));
    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(-4.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Lambertian>(Vec3(0.4f, 0.2f, 0.1f))));
    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(4.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Metal>(Vec3(0.7f, 0.6f, 0.5f), 0.0f)));

    // Generate BVH and add to world
    std::shared_ptr<BVH_Node> pBVH = std::make_shared<BVH_Node>(SceneObjects);
    _World.AddObject(pBVH);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void generateRTinOneWeekendSceneWithLight(HittableList& _World)
{
    HittableList SceneObjects;

    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(0.0f, -1000.0f, 0.0f), 1000.0f, std::make_shared<Lambertian>(Vec3(0.5, 0.5, 0.5))));

    int i = 1;
    for (int a = -11; a < 11; a++)
    {
        for (int b = -11; b < 11; b++)
        {
            auto choose_mat = getRandomReal();
            Vec3 center(a + 0.9f * getRandomReal(), 0.2f, b + 0.9f * getRandomReal());
            if ((center - Vec3(4.f, 0.2f, 0.f)).getLength() > 0.9f)
            {
                if (choose_mat < 0.8f)
                {
                    // diffuse
                    auto albedo = getRandomVec3() * getRandomVec3();
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Lambertian>(albedo)));
                }
                else if (choose_mat < 0.93f)
                {
                    // metal
                    auto albedo = getRandomVec3(.5f, 1.f);
                    auto fuzz = getRandomReal(0.f, .5f);
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Metal>(albedo, fuzz)));
                }
                else if (choose_mat < 0.98f)
                {
                    // light
                    auto color = getRandomVec3(.3f, 1.f);
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<DiffuseLight>(color)));
                }
                else
                {
                    // glass
                    SceneObjects.AddObject(std::make_shared<Sphere>(center, 0.2f, std::make_shared<Dielectric>(1.5f)));
                }
            }
        }
    }

    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(0.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Dielectric>(1.5f)));
    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(-4.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Lambertian>(Vec3(0.4f, 0.2f, 0.1f))));
    SceneObjects.AddObject(std::make_shared<Sphere>(Vec3(4.0f, 1.0f, 0.0f), 1.0f, std::make_shared<Metal>(Vec3(0.7f, 0.6f, 0.5f), 0.0f)));

    // Generate BVH and add to world
    std::shared_ptr<BVH_Node> pBVH = std::make_shared<BVH_Node>(SceneObjects);
    _World.AddObject(pBVH);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------


/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// SceneSelector class
/// ------------------------------------------------------------------------------------------------------------------------------------------------
SceneSelector::SceneSelector()
{
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Member Functions
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void SceneSelector::LoadScene(ESceneType _kSceneType, HittableList& _World) const
{
    switch (_kSceneType)
    {
    case SceneSelector::kSceneType_TestScene:
        generateTestScene(_World);
        break;
    case SceneSelector::kSceneType_RaytracingInOneWeekend:
        generateRTinOneWeekendScene(_World);
        break;
    case SceneSelector::kSceneType_RaytracingInOneWeekendWithLights:
        generateRTinOneWeekendSceneWithLight(_World);
        break;
    default:
        std::cerr << "SceneSelector::LoadScene: Scene type '" << _kSceneType << "' is not supported. Loading test scene instead." << std::endl;
        generateTestScene(_World);
        break;
    }
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
std::shared_ptr<SceneBackground> SceneSelector::getBackground(ESceneType _kSceneType) const
{
    switch (_kSceneType)
    {
    case SceneSelector::kSceneType_TestScene:
        return std::make_shared<SceneBackgroundEarthAndSky>();
        break;
    case SceneSelector::kSceneType_RaytracingInOneWeekend:
        return std::make_shared<SceneBackgroundEarthAndSky>();
        break;
    case SceneSelector::kSceneType_RaytracingInOneWeekendWithLights:
        return std::make_shared<SceneBackground>(Vec3(0.01, 0.01, 0.01));
        break;
    default:
        std::cerr << "SceneSelector::getBackground: Scene type '" << _kSceneType << "' is not supported. Using earth and sky background instead." << std::endl;
        return std::make_shared<SceneBackgroundEarthAndSky>();
        break;
    }
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
SceneSelector::CameraConfiguration SceneSelector::getCameraConfiguration(ESceneType _kSceneType) const
{
    CameraConfiguration CamConfig;

    switch (_kSceneType)
    {
    case SceneSelector::kSceneType_TestScene:
        CamConfig.vViewPos = Vec3(-3.0, 3.0, 2.0);
        CamConfig.vViewTarget = Vec3(0.0, 0.0, -1.0);
        CamConfig.fAperture = 0.2f;
        break;
    case SceneSelector::kSceneType_RaytracingInOneWeekend:
    case SceneSelector::kSceneType_RaytracingInOneWeekendWithLights:
        CamConfig.vViewPos = Vec3(13.f, 2.f, 3.f);
        CamConfig.vViewTarget = Vec3(0.f, 0.f, 0.f);
        CamConfig.fAperture = 0.1f;
        break;
    default:
        std::cerr << "SceneSelector::getCameraConfiguration: Scene type '" << _kSceneType << "' is not supported. Using default." << std::endl;
        CamConfig.vViewPos = Vec3(13.f, 2.f, 3.f);
        CamConfig.vViewTarget = Vec3(0.f, 0.f, 0.f);
        CamConfig.fAperture = 0.1f;
        break;
    }

    return CamConfig;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
