#include "Camera.h"
#include "Random.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
Camera::Camera(const Vec3& _vOrigin, const Vec3& _vDirection, Real _fVerticalFoV, Real _fAspectRatio, Real _fAperture, Real _fFocusDist) :
	m_fVerticalFoV(_fVerticalFoV),
	m_fAspectRatio(_fAspectRatio),
	m_fLensRadius(_fAperture * Real(0.5)),
	m_fFocusDist(_fFocusDist),
	m_vOrigin(_vOrigin),
	m_vDirection(_vDirection),
	m_vUp(0.0, 1.0, 0.0)
{
	RecalculateInternals();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
Ray Camera::getRay(Real _fXUV, Real _fYUV) const
{
	Vec3 vPointOnLens = m_fLensRadius * getRandomPointOnUnitDisk();
	vPointOnLens = m_vU * vPointOnLens.x + m_vV * vPointOnLens.y;
	return Ray(m_vOrigin + vPointOnLens, m_vCornerBottomLeft + _fXUV * m_vHorizontal + _fYUV * m_vVertical - m_vOrigin - vPointOnLens);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void Camera::RecalculateInternals()
{
	/// convert degrees to radians
	Real fTheta = m_fVerticalFoV * kPi / Real(180.0);
	Real fHalfHeightFrustum = tan(fTheta * Real(0.5));
	Real fHalfWidthFrustum = m_fAspectRatio * fHalfHeightFrustum;

	m_vW = -m_vDirection.getUnitVec();;
	m_vU = m_vUp.cross(m_vW).getUnitVec();
	m_vV = m_vW.cross(m_vU);

	m_vCornerBottomLeft = m_vOrigin - fHalfWidthFrustum * m_fFocusDist * m_vU - fHalfHeightFrustum * m_fFocusDist * m_vV - m_fFocusDist * m_vW;
	m_vHorizontal = m_vU * Real(2.0) * m_fFocusDist * fHalfWidthFrustum;
	m_vVertical = m_vV * Real(2.0) * m_fFocusDist * fHalfHeightFrustum;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
