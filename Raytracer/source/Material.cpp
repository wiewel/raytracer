#include "Material.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Functions
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Currently only the transition between air (n0 = 1.0) and other media is supported
/// _vNormalMedia0 should be inward facing when _vIncomingMedia0 points into direction of the surface
bool getRefractedRay(const Vec3& _vIncomingMedia0, const Vec3& _vNormalMedia0, Real _fN1overN0, Vec3& _vRefracted)
{
	Vec3 vUnitIncoming = _vIncomingMedia0.getUnitVec();
	Real fCosIncoming = vUnitIncoming.dot(_vNormalMedia0);
	Real fDiscriminant = Real(1.0) - _fN1overN0 * _fN1overN0 * (Real(1.0) - fCosIncoming * fCosIncoming);
	if (fDiscriminant > 0.0)
	{
		_vRefracted = _fN1overN0 * (vUnitIncoming - _vNormalMedia0 * fCosIncoming) - _vNormalMedia0 * sqrt(fDiscriminant);
		return true;
	}
	else
	{
		return false;
	}
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Polynomial approximation of the Fresnel factor by Christophe Schlick is returned
Real getFresnelFactor(Real _fCosineIncoming, Real _fRefractionIndex)
{
	Real fR0 = (Real(1.0) - _fRefractionIndex) / (Real(1.0) + _fRefractionIndex);
	fR0 = fR0 * fR0;
	return fR0 + (Real(1.0) - fR0) * Real(pow((Real(1.0) - _fCosineIncoming), 5));
}

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Lambertian
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool Lambertian::Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const
{
	Vec3 vScatterDir = _HitInfo.vHitNormal + getRandomUnitVec3();

	if (vScatterDir.isCloseToZero())
		vScatterDir = _HitInfo.vHitNormal;

	_ScatteredRay = Ray(_HitInfo.vHitPosition, vScatterDir);
	_vAttenuation = m_vAlbedoColor;
	return true;
}

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Metal
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool Metal::Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const
{
	Vec3 vReflected = _Ray.getDirection().getUnitVec().getReflectedVector(_HitInfo.vHitNormal);
	_ScatteredRay = Ray(_HitInfo.vHitPosition, vReflected + m_fRoughness * getRandomPointInUnitSphere());
	_vAttenuation = m_vAlbedoColor;
	return _ScatteredRay.getDirection().dot(_HitInfo.vHitNormal) > 0.0;
}

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Metal
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool Dielectric::Scatter(const Ray& _Ray, const HitInfo& _HitInfo, Vec3& _vAttenuation, Ray& _ScatteredRay) const
{
	/// Glass absorbs no light, therefore attenuation is always 1
	_vAttenuation = Vec3(1.0, 1.0, 1.0);

	Vec3 vOutwardNormal;
	Vec3 vRefracted;
	Real fN1overN0;

	Real fReflectionProbability;
	Real fCosineIncoming;

	if (_Ray.getDirection().dot(_HitInfo.vHitNormal) > 0.0)
	{
		vOutwardNormal = -_HitInfo.vHitNormal;
		fN1overN0 = m_fRefractionIndex;
		fCosineIncoming = m_fRefractionIndex * _Ray.getDirection().dot(_HitInfo.vHitNormal) / _Ray.getDirection().getLength();
	}
	else
	{
		vOutwardNormal = _HitInfo.vHitNormal;
		fN1overN0 = Real(1.0) / m_fRefractionIndex;
		fCosineIncoming = -_Ray.getDirection().dot(_HitInfo.vHitNormal) / _Ray.getDirection().getLength();
	}

	if (getRefractedRay(_Ray.getDirection(), vOutwardNormal, fN1overN0, vRefracted))
	{
		fReflectionProbability = getFresnelFactor(fCosineIncoming, m_fRefractionIndex);
	}
	else
	{
		fReflectionProbability = 1.0;
	}

	if (getRandomDouble() < fReflectionProbability)
	{
		Vec3 vReflected = _Ray.getDirection().getReflectedVector(_HitInfo.vHitNormal);
		_ScatteredRay = Ray(_HitInfo.vHitPosition, vReflected);
	}
	else
	{
		_ScatteredRay = Ray(_HitInfo.vHitPosition, vRefracted);
	}
	return true;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
