#include <memory>

#include "Raytracer.h"
#include "ImageFile.h"
#include "Vec3.h"
#include "Ray.h"
#include "MathFunc.h"
#include "Sphere.h"
#include "Camera.h"
#include "Random.h"

#include "RenderContext.h"
#include "Texture2D.h"
#include "FullscreenShader.h"

#include "Threading.h"
#include "Material.h"
#include "BVH.h"
#include "Light.h"

#include "SceneSelector.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
inline Vec3 Trace(const Ray& _Ray, Hittable& _World, const std::shared_ptr<SceneBackground> _pBackground, uint _uRecDepth)
{
    // When the recursion depth is reached end scene traversal and return black.
    if (_uRecDepth == 0u)
        return Vec3(0.0, 0.0, 0.0);

    // Trace into the scene with current ray and return the background if nothing is hit.
    HitInfo CurrentHit;
    if( _World.isHitting(_Ray, 0.001f, (std::numeric_limits<Real>::max)(), CurrentHit) == false )
        return _pBackground->Evaluate(_Ray);

    // Otherwise, lock the hit material...
    auto pHitMat = CurrentHit.pMaterial.lock();
    assertm(pHitMat != nullptr, "Trace function: pHitMat is nullptr. Abort.");
    if (pHitMat == nullptr)
        return Vec3();

    // ... and evaluate if it scatters the ray. If not, just return the emission component.
    Ray  ScatteredRay;
    Vec3 vAttenuation;
    Vec3 vEmitted = pHitMat->Emitted();

    if (pHitMat->Scatter(_Ray, CurrentHit, vAttenuation, ScatteredRay) == false)
        return vEmitted;

    // If the ray scatters, continue to traverse the scene in scattered direction and finally sum up all components.
    return vEmitted + vAttenuation * Trace(ScatteredRay, _World, _pBackground, _uRecDepth - 1u);
}

/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// Main 
/// ------------------------------------------------------------------------------------------------------------------------------------------------
int main(int argc, char** argv)
{
    const uint nx = 800u;
    const uint ny = 400u;
    const uint ns = 20u;
    const uint uNumThreads = 8u;
    const uint uMaxRecDepth = 10u;
    const SceneSelector::ESceneType kSelectedScene = SceneSelector::kSceneType_RaytracingInOneWeekend;

    /// Create context with window
    RenderContext Context;
    Context.Create(nx, ny, "Raytracer");

    /// Initialize render objects
    GLubyte* pRenderData = new GLubyte[nx * ny * 4u];
    Texture2D MainTex;
    FullscreenShader FullscreenShader;

    /// Create scene selector
    SceneSelector SceneLoader;

    /// Initialize raytracing scene
    HittableList World;
    SceneLoader.LoadScene(kSelectedScene, World);

    /// Setup camera
    SceneSelector::CameraConfiguration CamConfig = SceneLoader.getCameraConfiguration(kSelectedScene);

    Vec3 vViewDir = CamConfig.vViewTarget - CamConfig.vViewPos;
    Real fDistToFocus = vViewDir.getLength();

    HitInfo CurrentHit;
    if (World.isHitting(Ray(CamConfig.vViewPos, vViewDir), 0.001f, (std::numeric_limits<Real>::max)(), CurrentHit))
    {
        fDistToFocus = (CurrentHit.vHitPosition - CamConfig.vViewPos).getLength();
    }
    Camera Cam(CamConfig.vViewPos, vViewDir, 20.0, Real(nx) / Real(ny), CamConfig.fAperture, fDistToFocus);

    /// Define the render function 
    SpinLock Lock;
    std::function<void(uint,uint,uint,uint)> RenderFunc = [&] (uint _uStartX, uint _uEndX, uint _uStartY, uint _uEndY)
    {
        /// do actual raytracing
        for (uint j = _uStartY; j < _uEndY; ++j)
        {
            for (uint i = _uStartX; i < _uEndX; i++)
            {
                Vec3 vColor(0.0f, 0.0f, 0.0f);
                for (uint uSample = 0; uSample < ns; ++uSample)
                {
                    float u = Real(i + getRandomReal()) / Real(nx);
                    float v = Real(j + getRandomReal()) / Real(ny);

                    Ray Ray = Cam.getRay(u, v);
                    vColor += Trace(Ray, World, SceneLoader.getBackground(kSelectedScene), uMaxRecDepth);
                }

                vColor /= Real(ns);
                /// Gamma correction
                vColor = Vec3(sqrt(vColor.r), sqrt(vColor.g), sqrt(vColor.b));

                Lock.AcquireLock();

                pRenderData[(j * nx + i) * 4 + 0] = static_cast<GLubyte>(255.99f * vColor.r);
                pRenderData[(j * nx + i) * 4 + 1] = static_cast<GLubyte>(255.99f * vColor.g);
                pRenderData[(j * nx + i) * 4 + 2] = static_cast<GLubyte>(255.99f * vColor.b);

                Lock.ReleaseLock();
            }
        }
    };

    std::thread RayTraceThread[uNumThreads];
    for(uint uThreadIdx = 0; uThreadIdx < uNumThreads; ++uThreadIdx)
    {
        uint uStartX = uThreadIdx * (nx / uNumThreads);
        uint uEndX   = (uThreadIdx + 1) * (nx / uNumThreads);
        uint uStartY = 0;
        uint uEndY   = ny;
        std::cout << "Thread " << uThreadIdx << ": X[" << uStartX << ", " << uEndX << "]" << " Y[" << uStartY << ", " << uEndY << "]" << std::endl;
        if(uThreadIdx == uNumThreads - 1)
        {
            uEndX = nx;
            uEndY = ny;
        }
        RayTraceThread[uThreadIdx] = std::thread(RenderFunc, uStartX, uEndX, uStartY, uEndY);
    }

    while (!glfwWindowShouldClose(Context.GetWindow()))
    {
        int width, height;
        glfwGetFramebufferSize(Context.GetWindow(), &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);

        /// convert result to OpenGL image
        Lock.AcquireLock();
        MainTex.Create(nx, ny, GL_RGBA, GL_UNSIGNED_BYTE, pRenderData);
        Lock.ReleaseLock();

        MainTex.Activate(GL_TEXTURE0);
        FullscreenShader.Activate(0);
        FullscreenShader.Render();

        glfwSwapBuffers(Context.GetWindow());
        glfwPollEvents();
    }

    for(std::thread& CurThread : RayTraceThread)
    {
        CurThread.join();
    }

    //PPM image(nx, ny);
    //image.Fill(imgData);
    //image.Write("D:\\GIT\\gitlab\\raytracer\\test.ppm");

    /// Clean up
    if(pRenderData)
    {
        delete[] pRenderData;
    }

    return 0;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------

