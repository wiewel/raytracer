#include "Sphere.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool Sphere::isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT, HitInfo& _HitInfo) const
{
    Vec3 vOC = _Ray.getOrigin() - m_vCenter;
    Real fA = _Ray.getDirection().dot(_Ray.getDirection());
    Real fB = vOC.dot(_Ray.getDirection());
    Real fC = vOC.dot(vOC) - m_fRadius * m_fRadius;
    Real fDiscriminant = fB * fB - fA * fC;
    if (fDiscriminant > 0.0f)
    {
        /// First solution of quad eval: -
        Real fTmp = (-fB - sqrt(fDiscriminant)) / fA;
        if (fTmp > _fMinT&& fTmp < _fMaxT)
        {
            _HitInfo.fT = fTmp;
            _HitInfo.vHitPosition = _Ray.getPointAtParameter(_HitInfo.fT);
            _HitInfo.vHitNormal = (_HitInfo.vHitPosition - m_vCenter) / m_fRadius;
            _HitInfo.pMaterial = std::weak_ptr<Material>(m_pMaterial);
            return true;
        }
        /// Second solution of quad eval: +
        fTmp = (-fB + sqrt(fDiscriminant)) / fA;
        if (fTmp > _fMinT&& fTmp < _fMaxT)
        {
            _HitInfo.fT = fTmp;
            _HitInfo.vHitPosition = _Ray.getPointAtParameter(_HitInfo.fT);
            _HitInfo.vHitNormal = (_HitInfo.vHitPosition - m_vCenter) / m_fRadius;
            _HitInfo.pMaterial = std::weak_ptr<Material>(m_pMaterial);
            return true;
        }
    }
    return false;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
bool Sphere::getBoundingBox(AABB& _AABB) const
{
    _AABB = AABB(m_vCenter - Vec3(m_fRadius, m_fRadius, m_fRadius), m_vCenter + Vec3(m_fRadius, m_fRadius, m_fRadius));
    return true;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
