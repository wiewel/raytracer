#include "AABB.h"


/// ------------------------------------------------------------------------------------------------------------------------------------------------
/// isHitting implements the slab method to detect ray - box collisions.
bool AABB::isHitting(const Ray& _Ray, Real _fMinT, Real _fMaxT) const
{
	for (uint uDim = 0u; uDim < 3; ++uDim)
	{
		Real fInvD = Real(1.0) / _Ray.getDirection()[uDim];
		Real t0 = (m_vMinimum[uDim] - _Ray.getOrigin()[uDim]) * fInvD;
		Real t1 = (m_vMaximum[uDim] - _Ray.getOrigin()[uDim]) * fInvD;
		if (fInvD < Real(0.0))
			std::swap(t0, t1);
		_fMinT = t0 > _fMinT ? t0 : _fMinT;
		_fMaxT = t1 < _fMaxT ? t1 : _fMaxT;
		if (_fMaxT < _fMinT)
			return false;
	}
	return true;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void AABB::combineWith(const AABB& _Other)
{
	// Adjust minimum of AABB
	m_vMinimum.x = std::min(_Other.getMin().x, m_vMinimum.x);
	m_vMinimum.y = std::min(_Other.getMin().y, m_vMinimum.y);
	m_vMinimum.z = std::min(_Other.getMin().z, m_vMinimum.z);
	// Adjust maximum of AABB
	m_vMaximum.x = std::max(_Other.getMax().x, m_vMaximum.x);
	m_vMaximum.y = std::max(_Other.getMax().y, m_vMaximum.y);
	m_vMaximum.z = std::max(_Other.getMax().z, m_vMaximum.z);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------


