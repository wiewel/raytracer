#include "RenderContext.h"

#include <glad/gl.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
//#include "linmath.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
static void OpenGL_ErrorCallback(int _iError, const char* _sDescription)
{
    std::cerr << "GLFW Error: " << _sDescription << std::endl;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
static void OpenGL_KeyCallback(GLFWwindow* _pWindow, int _iKey, int _iScancode, int _iAction, int _iMods)
{
    if (_iKey == GLFW_KEY_ESCAPE && _iAction == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(_pWindow, GLFW_TRUE);
    }
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
// https://learnopengl.com/In-Practice/Debugging
void APIENTRY glDebugOutput(GLenum source, 
                            GLenum type, 
                            unsigned int id, 
                            GLenum severity, 
                            GLsizei length, 
                            const char *message, 
                            const void *userParam)
{
    // ignore non-significant error/warning codes
    if(id == 131169 || id == 131185 || id == 131218 || id == 131204) return; 

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " <<  message << std::endl;

    switch (source)
    {
        case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
        case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
        case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
    } std::cout << std::endl;

    switch (type)
    {
        case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break; 
        case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
        case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
        case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
        case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
        case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
        case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
    } std::cout << std::endl;
    
    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
        case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
        case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
        case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
    } std::cout << std::endl;
    std::cout << std::endl;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
RenderContext::RenderContext()
{
    glfwSetErrorCallback(OpenGL_ErrorCallback);

    if (glfwInit() == GLFW_FALSE)
    {
        std::cerr << "Error: GLFW init failed!" << std::endl;
        exit(EXIT_FAILURE);
    }

#ifdef __APPLE__
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    //// Always start with debug context for now
    //glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
RenderContext::~RenderContext()
{
    Destroy();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void RenderContext::Create(int _iResX, int _iResY, const std::string& _sWindowTitle)
{
    if(m_pWindow == nullptr)
    {
        m_pWindow = glfwCreateWindow(_iResX, _iResY, _sWindowTitle.c_str(), NULL, NULL);
        if (m_pWindow == nullptr)
        {
            std::cerr << "Error: creation of OpenGL context failed!" << std::endl;
            glfwTerminate();
            exit(EXIT_FAILURE);
        }
        glfwMakeContextCurrent(m_pWindow);

        if(gladLoadGL(glfwGetProcAddress) == 0)
        {
            std::cerr << "glad Error: terminating..." << std::endl;
            glfwTerminate();
            exit(EXIT_FAILURE);
        }

        // Unfortunately the debug context isn't supported on OpenGL 4.1
        int flags;
        glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
        if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS); 
            glDebugMessageCallback(glDebugOutput, nullptr);
            //glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
        }

        glViewport(0, 0, _iResX, _iResY);
        glfwSetKeyCallback(m_pWindow, OpenGL_KeyCallback);

        glfwSwapInterval(1);
    }
    std::cout << "Initialized OpenGL Context" << std::endl;
    std::cout << "\t Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "\t Resolution: " << _iResX << "x" << _iResY << std::endl;
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void RenderContext::Destroy()
{
    if (m_pWindow != nullptr)
    {
        glfwDestroyWindow(m_pWindow);
    }
    glfwTerminate();
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
