#include "ImageFile.h"

#include <iostream>
#include <fstream>
using namespace std;

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

/// ------------------------------------------------------------------------------------------------------------------------------------------------
template <class T>
void ImageFile<T>::Fill(vector<T>& _Data)
{
	assertm(_Data.size() == m_Data.size(), "Vector dimensions must match.");
	m_Data = vector<T>(_Data);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void PPM::Fill(vector<ColorRGB>& _Data)
{
    assertm(_Data.size() == m_Data.capacity(), "Vector dimensions must match.");
    m_Data = vector<ColorRGB>(_Data);
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
void PPM::Write(const string& _sFilePath) const
{
    ofstream imageFile(_sFilePath);
    assertm(imageFile.is_open(), "Unable to open image file.");

    if (imageFile.is_open())
    {
        imageFile << "P3\n" << m_iResX << " " << m_iResY << "\n" << m_iMaxColor << "\n";

        const float fMaxColor = static_cast<float>(m_iMaxColor) + 0.99f;

        for( const ColorRGB& pixel : m_Data )
        {
            float r = static_cast<float>(pixel.r);
            float g = static_cast<float>(pixel.g);
            float b = static_cast<float>(pixel.b);
            int ir = static_cast<int>(fMaxColor * r);
            int ig = static_cast<int>(fMaxColor * g);
            int ib = static_cast<int>(fMaxColor * b);
            imageFile << ir << " " << ig << " " << ib << "\n";
        }
        imageFile.close();
    }
}
/// ------------------------------------------------------------------------------------------------------------------------------------------------
